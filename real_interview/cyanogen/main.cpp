#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <set>
#include <queue>
#include <stack>
using namespace std;


class bitmap {
private:
    int mWidth;
    int mHeight;
    int** mColors;

public:
    bitmap(int width, int height) {
        mWidth = width;
        mHeight = height;
        
        mColors = new int*[width];
        for (int i = 0; i < height; i++) {
            mColors[i] = new int[height];
        }
    }

    int getPixel(int x, int y) {
        return mColors[x][y];
    }

    void setPixel(int x, int y, int color) {
        mColors[x][y] = color;
    }

    int getWidth() {
        return mWidth;
    }

    int getHeight() {
        return mHeight;
    }

    void printBitmap() {
        int width = getWidth();
        int height = getHeight();
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                cout << getPixel(i, j) << " ";
            }

            cout << "\n";
        }
    }
};


// Implementation Start (Feel free to use helper methods)


bitmap* navFillColor(bitmap* input, int x, int y, int color);

bitmap* fillWithColor(bitmap* input, int x, int y, int color) {
    int initColor = input->getPixel(x, y);
    if(initColor == color) return input;
    bitmap *newBitmap = new bitmap(input->getWidth(), input->getHeight());
    cout << "fill" << endl;
    navFillColor(newBitmap, x, y, color);
    return newBitmap;
}

bitmap* navFillColor(bitmap* input, int x, int y, int color) {
    if(input->getPixel(x, y) == color) return input;
    else input->setPixel(x, y, color);
    cout << "nav" << endl;
    int rows = input->getHeight();
    int cols = input->getWidth();
    
    cout << x << y << " " << color << endl;
    //Keep going navigation
    int upX = x, downX = x, leftX = x-1, rightX = x+1;
    int upY = y-1, downY = y+1, leftY = y, rightY = y;
    //left
    if(leftX >=0 && leftY >=0 && leftY < rows) fillWithColor(input, leftX, leftY, color);
    //right
    if(rightX < cols && rightY >= 0 && rightY < rows) fillWithColor(input, rightX, rightY, color);
    //up
    if(upY >= 0 && upX >= 0 && upX < cols) fillWithColor(input, upX, upY, color);
    //down
    if(downY < rows && downX >= 0 && downX < cols) fillWithColor(input, downX, downY, color);
    return input;
}

int main() {
    int x, y, color;

    int width, height;
    bitmap* input = new bitmap(3, 3);


    input->setPixel(0, 0, 0);
    input->setPixel(0, 1, 1);
    input->setPixel(0, 2, 0);
    input->setPixel(1, 0, 0);
    input->setPixel(1, 1, 1);
    input->setPixel(1, 2, 0);
    input->setPixel(2, 0, 1);
    input->setPixel(2, 1, 1);
    input->setPixel(2, 2, 0);

    cout << "str" << endl;
    bitmap* output = fillWithColor(input, 0, 0, 1);
    output->printBitmap();

    if (input == output) {
        cout << "Error: Same bitmap used!\n";
    }
    
    return 0;
}
