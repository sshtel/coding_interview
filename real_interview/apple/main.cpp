void memcopy(void *pSource, void *pDest, unsigned int numBytes)
{
    numBytes--;
    while(numBytes != 0){
        *((char*)(pDest) + numBytes) = (*((char*)(pSource) + numBytes));
        numBytes--;
    }
}

int main(){
    char a[3] = {0, };
    char b[3] = {1, };
    memcopy(a, b, 3);
    return 0;
}
