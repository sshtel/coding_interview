#include <iostream>
#include <string>
#include <sstream>

/*
 Implement a method to perform basic string compression using the count of repeated characters.

input:  aaabbcc
output: a3b2c2
 */

using namespace std;

string compressString(string & data){
    int size = data.size(); if(size == 0) return data;
    string resultString = "";
    int subCount = 1;
    char subChar = 0;
    for(int i = 0; i < size; ++i){
        char currChar = data.at(i);
        if( subChar == 0 ) {
            subChar = currChar;
            subCount = 1;
        }
        else if( subChar != currChar ){
            resultString += subChar;
            string numStr;  
            ostringstream ostr; 
            ostr << subCount;
            resultString += ostr.str();
//            resultString += std::to_string( subCount ); // C++11
            subCount = 1; subChar = currChar;
        }
        else if (subChar == currChar)  subCount++;
    }

    //last subStr
    resultString += subChar;
    string numStr;  
    ostringstream ostr; 
    ostr << subCount;
    resultString += ostr.str();

    if(resultString.size() < size) return resultString;
    else return data;
}

int main(){
    string input = "abcd";
    //string input = "aaaabbbcd";
    string compress = compressString(input);

    cout << compress << endl;
    

    return 0;
}
