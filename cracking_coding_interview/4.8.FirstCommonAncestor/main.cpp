#include <iostream>


class MyTree{
public:
    MyTree *root_;
    MyTree(MyNode *root){
        root_ = root;
    }

    void addNode(MyNode *node);
};

class MyNode{
public:
    MyNode(int value, MyNode *root){ value_ = value; root_ = root; } 
    int value_;
    MyNode *parent_;
    MyNode *root_;
    
    int getDepth(){
        if(this == root_) return 0;
        return getDepth(this->parent_) + 1;
    }
    
    MyNode* getCommonAncestor(MyNode *param){
        int myDepth = getDepth();
        int yourDepth = node->getDepth();
        MyNode *node1, *node2;
        if(myDepth > yourDepth){
            node1 = getNodeWithDepth(this, yourDepth);
            node2 = param;
        }
        else if(myDepth < yourDepth){
            node1 = getNodeWithDepth(param, myDepth);
            node2 = this;
        }
        else if (myDepth == yourDepth){
            node1 = this; node2 = param;
        }
        return findCommonAncestor(node1, node2);
    }
    MyNode *getNodeWithDepth(MyNode *node, int depth){
        if(depth == node->getDepth()) return node;
        else return getNodeWithDepth(node->parent_, depth);
    }

    MyNode *findCommonAncestor(MyNode *node1, MyNode *node2){
        if(node1 == node2) return node1;
        else return findCommonAncestor(node1->parent_, node2->parent);
    }
};
