#include <iostream>

class TreeNode{
public:
    TreeNode(int value) { value_ = value; }
    int value_;
    TreeNode *parent_;
    TreeNode *leftChild_;
    TreeNode *rightChild_;
    
    TreeNode *getNextNode(){
        if(rightChild_ == 0) return parent_->getNextNode(value_); //search for parent
        else return rightChild_->getNextNode(value_); //search for right child
    }
    TreeNode *getNextNode(int value){
        if(leftChild_ == 0 || rightChild_ == 0) return this; // there is no node between this node and original node
        else if(parent_ == 0) return this; // top node
        else if(value_ < value) return parent_->getNextNode(value); //search for parent
        else if(leftChild_->value_ > value) return leftChild_->getNextNode(value); // there is a node between this node and original node
        else return this; // found node!
    }
};

int main(){
    TreeNode node1(1);
    TreeNode node2(2);
    TreeNode node5(5);

    node5.leftChild_ = &node1;
    node1.parent_ = &node5;
    node5.rightChild_ = &node2;
    node2.parent_ = &node5;

    TreeNode *next = node1.getNextNode();
    std::cout << next->value_ << std::endl;

    return 0;
}
