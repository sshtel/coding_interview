#include <iostream>
#include <vector>
#include <list>

using namespace std;

class BuildNode{ 
    public:
        BuildNode(char project) {
            project_ = project;
        }

    public:
        char project_;
        std::list<BuildNode*> parents_;
        std::list<BuildNode*> children_;

    public:
        void addChild(BuildNode *node){
            children_.push_back(node);
        }
        void addDependency(BuildNode *node) {
            node->addChild(this);
            parents_.push_back<node>;
        }
        bool isRoot() {
            if (parents_.size() == 0 ) return true; 
            else return false;
        }
        void print() {
            std::cout << project_ << "";
        }
        void removeDependency(BuildNode *node){
            parents_.remove(node);
        }
        void searchChildren(){
            print();
            list<BuildNode*>::iterator itr = children_.begin();
            for(; itr != children_.end(); ++itr){
                *itr->print(); *itr->removeDependency(this);
                if(itr->parents_.size() == 0 ) {
                    itr->searchChildren(); // recursive
                }
                else {
                    continue;
                }
            }
        }
};

int main(){
    BuildNode a('a'); BuildNode b('b'); BuildNode c('c'); 
    BuildNode d('d'); BuildNode e('e'); BuildNode f('f'); 

    vector<BuildNode> nodes;
    nodes.push_back(a); nodes.push_back(b); nodes.push_back(c);
    nodes.push_back(d); nodes.push_back(e); nodes.push_back(f);

    d.addDependency(&a);
    b.addDependency(&f);
    d.addDependency(&b);
    a.addDependency(&f);
    c.addDependency(&d);

    vector<BuildNode>::iterator itr = nodes.begin();
    for(; itr != nodes.end(); ++itr){
        if(itr->isRoot()) {
            itr->searchChildren();
        }
    }

}
