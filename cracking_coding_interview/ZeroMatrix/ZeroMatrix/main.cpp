/*
 write an algorithm such that if an element in an MxN matrix is 0, its entire row and column are set to 0.

 
 */

#include <iostream>
#include <map>
using namespace std;

void zeroMatrix(int *srcMat, int m, int n, int *dstMat){
    map<int, bool> rowCheckMap;
    map<int, bool> colCheckMap;
    //check zero elements
    for( int y = 0; y < n; ++y){
        for( int x = 0; x < m; ++x){
            int idx = (y * m) + x;
            dstMat[idx] = srcMat[idx]; // copy element
            if( srcMat[idx] == 0) {
                rowCheckMap[y] = true;
                colCheckMap[x] = true;
            }
        }
    }

    //fill zero
    for( int y = 0; y < n; ++y){
        for( int x = 0; x < m; ++x){
            int idx = (y * m) + x;
            if( colCheckMap[x] == true ||  rowCheckMap[y] == true)
                dstMat[idx] = 0; // fill with zeroa

        }
    }
}

int main(){
    
    int m = 5, n = 3;
    int *arr = new int[m*n];
    int *dst = new int[m*n];

    for(int i = 0; i < m*n; ++i){
        arr[i] = 1;
    }

    arr[12] = 0;

    zeroMatrix(arr, m, n, dst);

    //fill zero for rows
    for( int y = 0; y < n; ++y){
        for( int x = 0; x < m; ++x){
            int idx = (y * m) + x;
            cout << dst[idx];
        }
        cout << endl;
    }

    return 0;
}
