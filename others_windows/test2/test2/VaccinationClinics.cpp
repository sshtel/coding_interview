#include "VaccinationClinics.hpp"
#include <iostream>
#include <string>
#include "string_tool.hpp"

int test_vaccination_clinic(){
	std::string input;
	std::getline(std::cin, input);
	
	std::vector<std::string> input_param;
	split(input, ' ', input_param);	
	int N = std::atoi(input_param.at(0).c_str());
	int B = std::atoi(input_param.at(1).c_str());

	std::vector<int> city_population;
	
	for (int i = 0; i < N; ++i){
		std::getline(std::cin, input);
		int population = std::atoi(input.c_str());
		city_population.push_back(population);
	}

	maxVaccination(N, B, city_population);

	return 0;
}

int maxVaccination(int city, int vaccinate_clinic, std::vector<int> &city_population){
	
	int totalPopulation = 0;
	
	for (int i = 0; i < city; ++i){
		int population = city_population.at(i);
		totalPopulation += population;
	}

	float threshold = (float)totalPopulation / (float)vaccinate_clinic;
	std::vector<float> city_population_rate;
	std::vector<int> numOfVaccinate;
	for (int i = 0; i < city; ++i){
		if (city_population.at(i) < threshold){
			// only 1 vaccinate clinic
			numOfVaccinate.push_back(1);
		}
		else{
			
		}
		city_population_rate.push_back((float)((float)city_population.at(0) / (float)totalPopulation));
		int quotient = totalPopulation / city_population.at(0);
			
	}


	return 0;
}
