#include <iostream>
#include <vector>
#include <list>
#include <string>
#include "rotate.hpp"
#include "string_tool.hpp"


using namespace std;



int rowSize;

class Point{
public:
	int data;
	int x;
	int y;

public:
	Point(int x, int y, int data){
		this->x = x;
		this->y = y;
		this->data = data;
	}
	Point(){}

	void print(){
		std::cout << "x:" << x << " y:" << y << " =" << data << std::endl;
	}
	int getData(){
		return data;
	}

	void rotate(){
		int half = rowSize / 2;
		if (rowSize == 1){
			return;
		}
		else if ((rowSize == 2) ||
			(half >= 2 && half % 2 == 0)){
			if (x < half  && y < half){
				//area 1
				if (x >= y){ x++; }
				else{ y--; }
			}
			else if (x >= half && y < half){
				//area 2 
				//�밢�� ��
				if (x + y >= rowSize - 1){
					y++;
				}
				else if (x + y < rowSize - 1){
					x++;
				}
			}
			else if (x >= half && y >= half){
				//area 3
				if (x <= y){ x--; }
				else { y++; }
			}
			else if (x < half && y >= half){
				//area 4
				if (x + y <= rowSize - 1){
					y--;
				}
				else if (x + y > rowSize - 1){
					x--;
				}
			}
		}
		else{
			//odd
			if (x == half && y == half){
				//do nothing
				int a = 1;
			}
			else if (x == half && y < half){ x++; }
			else if (x == half && y > half){ x--; }
			else if (y == half && x > half){ y++; }
			else if (y == half && x < half){ y--; }
			else if (x < half  && y < half){
				//1 area
				if (x >= y){ x++; }
				else{ y--; }
			}
			else if (x > half && y < half){
				//2
				int gap = rowSize - x;
				int mid_y = gap - 1;

				if (mid_y <= y){ y++; }
				else if (mid_y > y) { x++; }

			}
			else if (x > half && y > half){
				//3
				if (x <= y){ x--; }
				else if (x > y) { y++; }
			}
			else if (x < half && y > half){
				//4
				int gap = rowSize - y;
				int mid_x = gap - 1;

				if (mid_x <= x){ y--; }
				else if (mid_x > x) { x--; }
			}
		}
	}
};

void rotate_sample(){

	Point *p = 0;;


	int rSize = 0;
	cin >> rSize;
	std::string tempLine;
	std::getline(std::cin, tempLine);

	int arrSize = rSize * rSize;
	rowSize = rSize;

	p = new Point[arrSize];
	int *arrBuf = new int[arrSize];

	for (int y = 0; y < rowSize; ++y){

		std::getline(std::cin, tempLine);
		vector<string> stringVec;
		split(tempLine, ' ', stringVec);

		for (int x = 0; x < rowSize; ++x){
			int idx = y * rowSize + x;
			Point *point = p + idx;
			point->x = x; point->y = y; point->data = std::stoi(stringVec[x]);
			point->rotate();
			int newIdx = point->y * rowSize + point->x;
			arrBuf[newIdx] = point->data;
		}
	}

	for (int y = 0; y < rowSize; ++y){

		for (int x = 0; x < rowSize; ++x){
			int idx = y * rowSize + x;
			Point *point = p + idx;

			cout << arrBuf[idx] << " ";
		}
		cout << endl;
	}

}



void rotateSquare(list<int> squareSrc, list<int>& squareResult){
	if (squareSrc.size() == 1) {
		squareResult.push_back(squareSrc.back());
		return;
	}
	else if (squareSrc.size() == 0){
		return;
	}

	int row = sqrt(squareSrc.size());
	

	list<int> border;
	list<int> innerSquare;
	list<int> stack;
	

	for (int y = 0; y < row; ++y){
		for (int x = 0; x < row; ++x){
			int item = squareSrc.front();
			squareSrc.pop_front();
			if (y == 0){
				border.push_back(item);
			}
			else if (y > 0 && y < row - 1){
				if (x == 0){
					//stack
					stack.push_back(item);
				}
				else if (x == row - 1){
					border.push_back(item);
				}
				else{
					innerSquare.push_back(item);
				}
			}
			else if (y == row - 1){
				//stack
				stack.push_back(item);
			}
		}
	}

	//rotate inner square
	list<int> innerResult;
	rotateSquare(innerSquare, innerResult);
	
	//attach stack
	int stackSize = stack.size();
	for (int i = 0; i < stackSize; ++i){
		int item = stack.back();
		stack.pop_back();
		border.push_back(item);
	}
	
	//rotate border
	int item = border.back();
	border.pop_back();
	border.push_front(item);
	
	//make result
	for (int y = 0; y < row; ++y){
		for (int x = 0; x < row; ++x){
			if (y == 0){
				squareResult.push_back(border.front());
				border.pop_front();
			}
			else if (y > 0 && y < row - 1){
				if (x == 0){
					squareResult.push_back(border.back());
					border.pop_back();
				}
				else if (x == row - 1){
					squareResult.push_back(border.front());
					border.pop_front();
				}
				else{
					squareResult.push_back(innerResult.front());
					innerResult.pop_front();
				}
			}
			else if (y == row - 1){
				squareResult.push_back(border.back());
				border.pop_back();
			}
		}
	}


}



void rotate_sample_2(){

	Point *p = 0;;


	int rSize = 0;
	cin >> rSize;
	std::string tempLine;
	std::getline(std::cin, tempLine);

	int arrSize = rSize * rSize;
	rowSize = rSize;

	p = new Point[arrSize];
	int *arrBuf = new int[arrSize];

	list<int> inputSquare;
	list<int> resultSquare;

	for (int y = 0; y < rowSize; ++y){

		std::getline(std::cin, tempLine);
		vector<string> stringVec;
		split(tempLine, ' ', stringVec);

		for (int x = 0; x < rowSize; ++x){
			int num = std::stoi(stringVec[x]);
			inputSquare.push_back(num);
		}
	}

	rotateSquare(inputSquare, resultSquare);


	for (int y = 0; y < rowSize; ++y){
		for (int x = 0; x < rowSize; ++x){
			cout << resultSquare.front() << " ";
			resultSquare.pop_front();
		}
		cout << endl;
	}



}