#include "reverse_words.hpp"

void reverse_words(char *src, int size){
	reverse_char(src, 0, size);

	int start_point = 0;
	int end_point = 0;
	for (int i = 0; i <= size; ++i){
		if (src[i] == ' ' || src[i] == 0){
			reverse_char(src, start_point, i);
			start_point = i + 1;
		}
	}

}

void reverse_char(char *src, int start, int end){
	char bufA, bufB;
	int size = end - start;
	end = end - 1;
	for (int i = 0; i < size / 2; ++i, start++, end--){
		bufA = src[start];
		bufB = src[end];
		src[start] = bufB;
		src[end] = bufA;
	}
}

