#ifndef __SUPER_UGLY_H
#define __SUPER_UGLY_H

#include <set>
#include <vector>
using namespace std;

class SuperUgly {
public:
	int nthSuperUglyNumber(int n, vector<int>& primes) {
		int k = primes.size(); //size of prime
		int max = n * k;
		set<int> ugly;
		ugly.insert(1);

		for (int i = 2; i < n + 1; ++i){
			if (isCorrectDivider(i, primes)){
				ugly.insert(i);
			}
		}
		return *ugly.rbegin();
	}

	bool isCorrectDivider(int divider, vector<int>& primes){
		vector<int>::iterator itr = primes.begin();
		for (; itr != primes.end(); ++itr){
			if (divider == *itr) return true;
			else if (divider / *itr == 0) return false;
			else if (divider % *itr != 0) continue;
			else return isCorrectDivider(divider / *itr, primes);
		}
		return false;
	}
};

#endif