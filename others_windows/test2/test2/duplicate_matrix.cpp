#include "duplicate_matrix.hpp"
#include "string_tool.hpp"
using namespace std;

bool isDuplicateEntry(std::vector<int> &matrix, int width, int k){

	int maxShift = width - k + 1;

	for (int y = 0; y < maxShift; ++y){
		for (int x = 0; x < maxShift; ++x){
			int idx = y * width + x;

			{
				std::vector<int> checkList;

				for (int sy = 0; sy < k; ++sy){
					for (int sx = 0; sx < k; ++sx){

						int small_idx = (y + sy) * width + (x + sx);

						int value = matrix.at(small_idx);
						checkList.push_back(value);

					}
				}

				for (int i = 0; i < checkList.size(); ++i){
					int value = checkList.at(i);
					for (int j = i + 1; j < checkList.size(); ++j){
						if (checkList.at(j) == value) return true;
					}
				}

			}

		}
	}

	return false;
}

int duplicate_sample(){

	int line = 0;
	std::string tempLine;
	std::cin >> line;

	int *mat;

	std::vector<int> matrix;
	std::getline(std::cin, tempLine);
	for (int i = 0; i < line; ++i){

		std::getline(std::cin, tempLine);


		std::vector<std::string> elems;

		split(tempLine, ' ', elems);

		for (auto& s : elems)
		{
			int result;
			stringstream(s) >> result;
			matrix.push_back(result);
		}
	}

	int kVal = 0;
	std::cin >> kVal;


	if (isDuplicateEntry(matrix, line, kVal))
		std::cout << "YES";
	else
		std::cout << "NO";

	return 0;
}