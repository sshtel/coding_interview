#ifndef __TWO_SUM_H
#define __TWO_SUM_H

#include <vector>
#include <iostream>
#include <algorithm>
#include <map>

using namespace std;

class Solution {
public:
	vector<int> twoSum(vector<int>& nums, int target) {

		int size = nums.size();
		vector<int> result;

		for (int i = 0; i < size; ++i){
			int src = nums.at(i);
			int toFind = target - src;

			for (int j = i + 1; j < size; ++j){
				if (toFind == nums.at(j)){
					result.push_back(i + 1);
					result.push_back(j + 1);
					return result;
				}
			}
		}
		return result;
	}


	vector<int> twoSum2(vector<int>& nums, int target) {

		int size = nums.size();
		vector<int> sorted = nums;
		std::sort(sorted.begin(), sorted.end());
		vector<int> result;



		int val1, val2;
		int ret = binary_search(sorted, target, &val1, &val2);

		if (ret != -1){
			vector<int>::iterator itr1 = std::find(nums.begin(), nums.end(), val1);

			if ((*itr1 == 0 && target == 0) ||
				val1 == val2
				){
				vector<int>::iterator itr2 = std::find(itr1 + 1, nums.end(), val1);
				int idx1 = std::distance(nums.begin(), itr1);
				int idx2 = std::distance(nums.begin(), itr2);
				result.push_back(idx1 + 1);
				result.push_back(idx2 + 1);
				std::sort(result.begin(), result.end());
				return result;
			}

			vector<int>::iterator itr2 = std::find(nums.begin(), nums.end(), val2);

			int idx1 = std::distance(nums.begin(), itr1);
			int idx2 = std::distance(nums.begin(), itr2);
			result.push_back(idx1 + 1);
			result.push_back(idx2 + 1);
		}
		std::sort(result.begin(), result.end());
		return result;
	}

	int binary_search(vector<int>& sorted, int target, int *val1, int *val2){
		int half_target = target / 2;
		vector<int>::iterator itr_center;
		vector<int>::iterator itr = sorted.begin();
		for (; itr != sorted.end(); ++itr){
			if (*itr >= half_target){
				itr_center = itr;
				break;
			}
		}


		vector<int>::iterator itr_downward = itr_center;
		vector<int>::iterator itr_upward = itr_center + 1;
		if (itr_downward == sorted.end() - 1){
			itr_downward = itr_center - 1;
			itr_upward = itr_center;
		}

		bool isDown = false;
		for (int i = 0; i < sorted.size(); ++i){
			int sum = *itr_downward + *itr_upward;
			if (sum == target) {
				*val1 = *itr_downward; *val2 = *itr_upward;
				return 0;
			}
			else if (sum > target){
				if (itr_downward == sorted.begin()){
					itr_upward--;
				}
				else if (*itr_upward > target){
					itr_upward--;
					itr_downward--;
				}
				else{
					itr_downward--;
				}
			}
			else if (sum < target){
				if (itr_upward == sorted.end() - 1){
					itr_downward++;
				}
				else{
					itr_upward++;
				}

			}
		}
		return -1;
	}
};

#endif