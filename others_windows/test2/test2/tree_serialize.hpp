#ifndef __TREE_SERIALIZE_H
#define __TREE_SERIALIZE_H

#include <string>
#include <vector>
#include <sstream>

using namespace std;

struct TreeNode {
	int val;
	TreeNode *left;
	TreeNode *right;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Codec {
public:

	// Encodes a tree to a single string.
	string serialize(TreeNode* root) {
		
		string ret = serialize_recursive(root);
		
		std::string retVal = "["; 
		retVal += ret.substr(0, ret.length() - 1);
		retVal += "]";

		return retVal;
	}

	string serialize_recursive(TreeNode* root) {
		if (root == NULL) return "";
		
		std::string ret = "";
		ret += std::to_string(root->val); ret += ",";

		if (root->left == NULL && root->right == NULL) { return ret; }

		if (root->left != 0){
			ret += serialize_recursive(root->left);
		}
		else{
			ret += "null,";
		}
		if (root->right != 0){
			ret += serialize_recursive(root->right);
		}
		else{
			ret += "null,";
		}
		return ret;
	}

	// Decodes your encoded data to tree.
	TreeNode* deserialize(string data) {
		std::size_t pos_begin = data.find('[');
		std::size_t pos_end = data.find(']');

		data = data.substr(pos_begin + 1, data.size() - 2);

		std::vector<string> strVec;
		split(data, ',', strVec);

		if (strVec.size() == 0) return NULL;

		TreeNode *rootNode = new TreeNode(stoi(strVec.at(0)));
		deserialize(rootNode, rootNode, strVec, 1);

		return rootNode;
	}

	std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
		std::stringstream ss(s);
		std::string item;
		while (std::getline(ss, item, delim)) {
			elems.push_back(item);
		}
		return elems;
	}

	TreeNode* deserialize(TreeNode* rootNode, TreeNode *parent, vector<string> &strVec, int idx){
		int size = strVec.size();
		if (size == idx) { return rootNode; }

		
		string item = strVec.at(idx);
		


		if (item == "null"){
			if (parent->left != NULL){
				parent->right = NULL;
			}
			else{
				parent->left = NULL;
			}
			deserialize(rootNode, parent, strVec, idx+1);
		}
		else{
			//if value, LEFT CHILD!!
			if (size == 2){
				TreeNode *node = new TreeNode(stoi(item));
				parent->left = node;
				return rootNode;
			}
			else if (size == 3){
				//only one item
				TreeNode *node = new TreeNode(stoi(item));
				parent->right = node;
				return rootNode;
			}
			else if (size == idx + 1){
				//only one item
				TreeNode *node = new TreeNode(stoi(item));
				parent->right = node;
				return rootNode;
			}
			string next = strVec.at(idx + 1);
			string next2 = strVec.at(idx + 2);

			TreeNode *node = new TreeNode(stoi(item));

			if (next == "null" && next2 == "null"){
				//leaf
				if (parent->left == NULL){
					parent->left = node;
				}
				else{
					parent->right = node;
				}				
				deserialize(rootNode, parent, strVec, idx + 3);
			}
			else if (next == "null"){
				//left is NULL, right is item
				parent->left = node;
				deserialize(rootNode, parent, strVec, idx + 2);
			}
			else{
				//left is item
				parent->left = node;
				deserialize(rootNode, parent, strVec, idx + 1);
			}
		}
		return rootNode;
	}

};

void test_tree_serialize();

#endif