#include "tree_serialize.hpp"
#include <iostream>

void test_tree_serialize(){
	Codec c;


	//c.deserialize("[1,2,null,null,3,null,null]");
	//c.deserialize("[1,2,null,null]");
	//TreeNode *tree = c.deserialize("[1,2,null,null,null]");
	TreeNode *tree = c.deserialize("[1,-1,2,null,null,3]");
	//TreeNode *tree = c.deserialize("[1, -1, 2, null, null, 3]");
	
	
	std::cout << c.serialize(tree) << std::endl;
}