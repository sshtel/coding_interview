#ifndef __REVERSE_WORDS_H
#define __REVERSE_WORDS_H

void reverse_words(char *src, int size);

void reverse_char(char *src, int start, int end);

#endif