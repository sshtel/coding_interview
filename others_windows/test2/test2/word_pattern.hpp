#ifndef __WORD_PATTERN_H
#define __WORD_PATTERN_H
#include <vector>
#include <string>
#include <map>
#include <sstream>
#include "string_tool.hpp"

using namespace std;

class WordPattern {
public:


	bool wordPattern(string pattern, string str) {

		typedef std::vector<string> VecString;
		std::map<char, VecString> patternMap;


		std::vector<std::string> strings;
		split(str, ' ', strings);

		int patternSize = pattern.size();

		if (patternSize != strings.size()) return false; //check pattern size

		for (int i = 0; i < patternSize; ++i){
			char c = pattern.at(i);
			string word = strings.at(i);
			patternMap[c].push_back(word);
		}

		{
			//verification duplicated vectors
			string prevString = "";
			std::map<char, VecString>::iterator patternItr = patternMap.begin();
			for (; patternItr != patternMap.end(); ++patternItr){
				VecString vecStr = patternItr->second;
				VecString::iterator itr = vecStr.begin();
				if (prevString == "") {
					prevString = *itr; continue;
				}
				if (prevString == *itr) {
					//duplicated vectors!!
					return false;
				}
			}
		}
		{
			//verification
			std::map<char, VecString>::iterator patternItr = patternMap.begin();
			for (; patternItr != patternMap.end(); ++patternItr){
				VecString vecStr = patternItr->second;
				VecString::iterator itr = vecStr.begin();
				string first = *itr;
				++itr;
				for (; itr != vecStr.end(); ++itr){
					if (*itr != first) return false;
				}
			}
		}
		//no problem
		return true;
	}
};


void word_pattern();

#endif