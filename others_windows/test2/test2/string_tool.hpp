#ifndef __STRING_TOOL_H
#define __STRING_TOOL_H
#include <vector>
#include <string>
#include <sstream>


std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);

#endif