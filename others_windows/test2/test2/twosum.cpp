#include "twosum.hpp"
using namespace std;


int two_sum(){
	Solution sol;

	vector<int> nums;
	nums.push_back(2);
	nums.push_back(1);
	nums.push_back(9);
	nums.push_back(4);
	nums.push_back(4);
	nums.push_back(56);
	nums.push_back(90);
	nums.push_back(3);
	vector<int> result = sol.twoSum2(nums, 8);


	vector<int>::iterator itr = result.begin();
	for (; result.end() != itr; ++itr){
		cout << *itr << endl;
	}


	return 0;
}

