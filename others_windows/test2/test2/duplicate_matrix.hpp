#ifndef __DUPLICATE_MATRIX_H
#define __DUPLICATE_MATRIX_H
#include <iostream>
#include <vector>


int duplicate_sample();

bool isDuplicateEntry(std::vector<int> &matrix, int width, int k);

#endif
