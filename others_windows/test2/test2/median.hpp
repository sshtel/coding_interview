#ifndef __MEDIAN_H
#define __MEDIAN_H


#include <iostream>
#include <vector>
#include <algorithm>
#include <list>

class MedianFinder {
private:
	std::list<int> stream_;

public:

	// Adds a number into the data structure.
	void addNum(int num) {
		if (stream_.size() == 0) { stream_.push_back(num); return; }
		std::list<int>::iterator itr = stream_.begin();
		for (; itr != stream_.end(); ++itr){
			if (*itr >= num) stream_.insert(itr, num);
		}
		//std::sort(stream_.begin(), stream_.end());
	}

	// Returns the median of current data stream
	double findMedian() {

		int size = stream_.size();
		if (size == 1){
			return stream_.back();
		}
		else if (size % 2 != 0) {
			int mid = size / 2 + 1;
			std::list<int>::iterator itr = stream_.begin();
			for (int i = 0; i < mid; ++i){
				itr++;
			}
			int value = *itr;
			return (double)value;
		}
		else{
			int mid = (size / 2);
			std::list<int>::iterator itr = stream_.begin();
			for (int i = 0; i < mid; ++i){
				itr++;
			}
			int value_1 = *itr;
			itr++;
			int value_2 = *itr;
			return (double)((value_1 + value_2) / 2);
		}

	}
};


int median_sample();

#endif