/*
rotate Array 2
==============
*/

#include <iostream>

void rotate(int[][] m){
  // 

  if (m[0].length != m.length) {
    System.out.println("ERROR");
  }
  int size = m.length;
  int half = size/2;

  int[][] org = new int[size][size];
  for(int i=0;i<size;i++){
    Arrays.copy(org[i], m[i]);
  }
      
  for(int i=0;i<=half;i++){
    int x = i;
    int y = i;
    int delta=i*2;
    int ex;
    int ey;

    // for x,y;
    m[y][x] = org[y-1][x];
    // .-----------
    for(int j=x+1;j<size-delta;j++){
      m[y][j] = org[y][j-1];      
    }
    m[y-1][i] = org[y][size-delta];
    // right side
    for(int j=1;j<size;j++){
      m[j][size-delta] = org[j-1][size-delta];      
    }
    
    // bottom side
    m[ey][ex]= org[ey-1][ex];
    for(int j=ex-1;j>=0;j--){
      m[ey][j] = org[ey][j+1];      
    }
    
    // left side
    m[ey][x]= org[ey-1][ex];
    for(int j=ex-1;j>=0;j--){
      m[ey][j] = org[ey][j+1];
    }

    
  }
  
  
}

//http://stackoverflow.com/questions/42519/how-do-you-rotate-a-two-dimensional-array

int main(){
    int arr[2][2] = { {1, 2}, {3, 4} };

    rotate(arr);
    return 0;
}


