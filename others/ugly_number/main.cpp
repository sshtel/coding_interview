#include <iostream>
#include <vector>
using namespace std;

class Solution {
    public:
        int nthSuperUglyNumber(int n, vector<int>& primes) {
            int count = 1;
            int num = 2;
            if(n == 1) return 1;

            do{
                bool isUgly = false;
                vector<int>::iterator itr = primes.begin();
                for(; itr != primes.end(); ++itr){
                    if(num < *itr) continue;
                    else if(num == *itr) { isUgly = true; break; }
                    else if(num % *itr == 0) { isUgly = true; break; }
                    else continue;
                }
                if(isUgly) {
                    count++;
                    cout << "isUgly : " << count << " " << num << endl;
                    if(count == n) return num;
                }
                cout << " " << count << " " << num << endl;
                num++;
            } while(count != n);
            return num;
        }
};

int main(){
    Solution s;
    std::vector<int> primes;
    primes.push_back(2);
    s.nthSuperUglyNumber(4, primes);
    return 0;
}
