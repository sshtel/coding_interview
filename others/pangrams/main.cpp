/*
 *
 sample

 We promptly judged antique ivory buckles for the next prize    
 * */

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    
    string input;
    getline(cin, input); 
    int ascii[256] = {0, };
    
    for(int i = 0; i < input.length(); ++i){
        char c = input.c_str()[i];
        cout << c << endl;
        if(c >= 'a') { c = (c - 'a') + 'A'; }
        ascii[c] = 1;
        
    }
    
    int count = 0;
    for(int i = 0; i < 26; ++i){
        count += ascii[i + 'A'];
    }
    
    if(count == 26) { cout << "pangram"; }
    else { cout << "not pangram"; }
    return 0;
}
