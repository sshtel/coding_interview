#include <iostream>
using namespace std;

bool isUglyNumber(int num);
int nthUglyNumber(int n) {
    if(n <= 6) return n;
    int num = 7; //start from 7
    int ret = num;
    int count = 7;
    while(count < n){
        cout << "cnt:" << count << endl;
        if(isUglyNumber(num)){
            ret = num;
            count++;
            cout << "ulgy!  num:" << num << " cnt:" << count << endl;
        }
        else{
            num++;
        }
    }
    return ret;
}

bool isUglyNumber(int num){
    do{

        if(num % 7 == 0) { return false; }
        else if(num % 11 == 0) { return false; }
        else if(num % 13 == 0) { return false; }
        else if(num % 17 == 0) { return false; }
        else if(num % 19 == 0) { return false; }
        else if(num % 23 == 0) { return false; }

        if(num % 2 == 0) { num /= 2; continue; }
        if(num % 3 == 0) { num /= 3; continue; }
        if(num % 5 == 0) { num /= 5; continue; }

        if(num == 1) { return true; }
        else { return false; }
    } while(1);
}

int main(){
    int ret = nthUglyNumber(7);
    cout << ret << endl;
    return 0;
}
