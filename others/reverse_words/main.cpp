
/*
Question 

I give a string buffer, this buffer length is very huge, almost 8 gigabyte.
This string buffer has many words or just only one word.
The system has not enough heap memory. It has only 1 gigabyte.
You must reversing all words in buffer.

for example,

this is test string
=>
string test is this

input string is char array

*/
#include <iostream>
#include <string>

void reverse_char(char *src, int start, int end);
void reverse_words(char *src, int size);

void reverse_words(char *src, int size){
	reverse_char(src, 0, size);

	int start_point = 0;
	int end_point = 0;
	for (int i = 0; i <= size; ++i){
		if (src[i] == ' ' || src[i] == 0){
			reverse_char(src, start_point, i);
			start_point = i + 1;
		}
	}

}

void reverse_char(char *src, int start, int end){
	char bufA, bufB;
	int size = end - start;
	end = end - 1;
	for (int i = 0; i < size / 2; ++i, start++, end--){
		bufA = src[start];
		bufB = src[end];
		src[start] = bufB;
		src[end] = bufA;
	}
}

int main(){
	char str[100] = "this is the string hahaha";
	reverse_words(str, strlen(str));
	std::cout << str;
	return 0;
}
