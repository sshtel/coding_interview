/**
 *  power(2, 1) = 2
 *   power(2, 2) = 4
 *    power(2, 3) = 8
 *       return power(2, 1) * power(2, 2) = 2 * 4 = 8
 *        power(2, 4) = 16
 *           return power(2, 2) * power(2, 2) = 4 * 4 = 16
 *              
 *                 base^8,base^4,base^2,base
 *                    
 *                       */

#include <iostream>
using namespace std;

int pow2(int base, int power){
    int r =1;
    while(power>0){
        if((power&1)>0){
            r*=base;
        }
        base*=base;
        power = power >> 1;
    }
    return r;
}

int main(){
    int result = pow2(2, 10);
    cout << result << endl;

}
